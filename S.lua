local S = {}

--[[------------------------------------------------------------------------- --
--
--  This module is a MIN/BTN ACR designed to run
--  the best rotations for a given target
--  (collectables, aethersands, quantity, quality).
--  It is also planned to support picking up exact quantities
--  of a material (within reason) to save inventory space.
--
--]]-------------------------------------------------------------------------


-- -------------------------------------------------------------------------- --
--                                Internal Info                               --
-- -------------------------------------------------------------------------- --

S._VERSION = '1'
S._NAME = 'Supplier'
S._DESCRIPTION = 'Miner and Botanist ACR'
S._URL = ''

d('[Anon Addons] Supplier Loaded')

-- -------------------------------------------------------------------------- --
--                              Module Variables                              --
-- -------------------------------------------------------------------------- --

S.GUI = {
    windowOpen = false,
    windowVisible = false,
    toggleVisible = true,
    name = 'Supplier',
}

S.classes = {
    [FFXIV.JOBS.MINER] = true,
    [FFXIV.JOBS.BOTANIST] = true,
}

S.settings = {
    
}

S.actions = {

}

S.rotations = {
    aethersand = {
        0 = function () then
            if Player.gp.current > 400 then
                S.state.step = 'F1'
            else
                S.state.step = 'E1'
            end
        end,
        F1 = function () 
            if not HasBuff(Player, 757) then
                if S.actions['scrutiny']:IsReady(Player.id) then
                    S.actions['scrutiny']:Cast(Player)
                    S.state.step = 'F2'
                end                        
            end
        end,
        F2 = function() 
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'F3'
            end 
        end,
        F3 = function () 
            if not HasBuff(Player, 757) then
                if S.actions['scrutiny']:IsReady(Player.id) then
                    S.actions['scrutiny']:Cast(Player)
                    S.state.step = 'F4'
                end                        
            end
        end,
        F4 = function() 
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'F5'
            end 
        end,
        F5 = function ()
            collectability = GetControl('GatheringMasterpiece'):GetRawData()[5].value
            if collectability == 1000 then 
                S.state.step = 'collect'
            elseif collectability >= 850 then
                S.state.step = 'F6'
            elseif collectability == 800 then
                if HasBuff(Player, 2418) then
                    S.state.step = 'F6'
                else
                    S.state.step = 'F7'
                end
            end        
        end,
        F6 = function ()
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'collect'
            end
        end,
        F7 = function ()
            if S.actions['scour']:IsReady(Player.id) then
                S.actions['scour']:Cast(Player)
                S.state.step = 'collect'
            end        
        end,
        E1 = function () 
            if S.actions['scour']:IsReady(Player.id) then
                S.actions['scour']:Cast(Player)
                S.state.step = 'E2'
            end
        end,
        E2 = function ()
            if HasBuff(Player, 2418) then
                S.state.step = 'E3'
            else
                S.state.step = 'E4'
            end
        end,
        E3 = function ()
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'collect'
            end 
        end,
        E4 = function () 
            if S.actions['scour']:IsReady(Player.id) then
                S.actions['scour']:Cast(Player)
                S.state.step = 'collect'
            end
        end,
        collect = function ()
            UseControlAction('GatheringMasterpiece','Collect')
        end
    },
    collectable = {
        0 = function () then
            if Player.gp.current > 400 then
                S.state.step = 'F1'
            else
                S.state.step = 'E1'
            end
        end,
        F1 = function () 
            if not HasBuff(Player, 757) then
                if S.actions['scrutiny']:IsReady(Player.id) then
                    S.actions['scrutiny']:Cast(Player)
                    S.state.step = 'F2'
                end                        
            end
        end,
        F2 = function() 
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'F3'
            end 
        end,
        F3 = function () 
            if not HasBuff(Player, 757) then
                if S.actions['scrutiny']:IsReady(Player.id) then
                    S.actions['scrutiny']:Cast(Player)
                    S.state.step = 'F4'
                end                        
            end
        end,
        F4 = function() 
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'F5'
            end 
        end,
        F5 = function ()
            collectability = GetControl('GatheringMasterpiece'):GetRawData()[5].value
            if collectability == 1000 then 
                S.state.step = 'collect'
            elseif collectability >= 850 then
                S.state.step = 'F6'
            elseif collectability == 800 then
                if HasBuff(Player, 2418) then
                    S.state.step = 'F6'
                else
                    S.state.step = 'F7'
                end
            end        
        end,
        F6 = function ()
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'collect'
            end
        end,
        F7 = function ()
            if S.actions['scour']:IsReady(Player.id) then
                S.actions['scour']:Cast(Player)
                S.state.step = 'collect'
            end        
        end,
        E1 = function () 
            if S.actions['scour']:IsReady(Player.id) then
                S.actions['scour']:Cast(Player)
                S.state.step = 'E2'
            end
        end,
        E2 = function ()
            if HasBuff(Player, 2418) then
                S.state.step = 'E3'
            else
                S.state.step = 'E4'
            end
        end,
        E3 = function ()
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'E5'
            end 
        end,
        E4 = function () 
            if S.actions['scour']:IsReady(Player.id) then
                S.actions['scour']:Cast(Player)
                S.state.step = 'E5'
            end
        end,
        E5 = function ()
            if HasBuff(Player, 2418) then
                S.state.step = 'E6'
            else
                S.state.step = 'E7'
            end
        end,
        E6 = function ()
            if S.actions['meticulous']:IsReady(Player.id) then
                S.actions['meticulous']:Cast(Player)
                S.state.step = 'collect'
            end 
        end,
        E7 = function () 
            if S.actions['scour']:IsReady(Player.id) then
                S.actions['scour']:Cast(Player)
                S.state.step = 'collect'
            end
        end,
        collect = function ()
            UseControlAction('GatheringMasterpiece','Collect')
        end
    }
}

S.state = {
    step = 0
}

-- -------------------------------------------------------------------------- --
--                               Initialization                               --
-- -------------------------------------------------------------------------- --

-- The OnLoad() function is fired when a profile is prepped and loaded by ACR.
function S.OnLoad()
    d('[Anon Addons] Supplier Initializing')
    if Player.job == FFXIV.JOBS.BOTANIST then
        S.actions['scour'] = ActionList:Get(1,22186)
        S.actions['brazen'] = ActionList:Get(1,22187)
        S.actions['meticulous'] = ActionList:Get(1,22188)
        S.actions['scrutiny'] = ActionList:Get(1,22189)
    end
    if Player.job == FFXIV.JOBS.MINER then
        S.actions['scour'] = ActionList:Get(1,22182)
        S.actions['brazen'] = ActionList:Get(1,22183)
        S.actions['meticulous'] = ActionList:Get(1,22184)
        S.actions['scrutiny'] = ActionList:Get(1,22185)   
    end 
end

-- -------------------------------------------------------------------------- --
--                                  Rendering                                 --
-- -------------------------------------------------------------------------- --

-- The OnOpen() function is fired when a user pressed "View Profile Options" on the main ACR window.
function S.OnOpen()
    -- Set our GUI table //open// variable to true so that it will be drawn.
    S.GUI.windowOpen = true
end

-- The Draw() function provides a place where a developer can show custom options.
function S.Draw()
    if (S.GUI.windowOpen) then
        GUI:SetNextWindowPosCenter(GUI.SetCond_FirstUseEver)
        S.GUI.windowVisible, S.GUI.windowOpen = GUI:Begin(S.GUI.name .. '##SAcrOptions', S.GUI.windowOpen,
            GUI.WindowFlags_AlwaysAutoResize)
        if (GUI:Button('Show')) then
            S.GUI.toggleVisible = true
        end
        GUI:End()
    end
    if (S.GUI.toggleVisible) then
        GUI:Begin('##SToggleButtons', S.GUI.toggleVisible,
            GUI.WindowFlags_AlwaysAutoResize + GUI.WindowFlags_NoTitleBar)
        GUI:End()
    end
end

-- -------------------------------------------------------------------------- --
--                                    Logic                                   --
-- -------------------------------------------------------------------------- --

function S.OnUpdate(event, tickcount)
    if(FFXIV_Common_BotRunning) then
        if not IsControlOpen('GatheringMasterpiece') 
        and not IsControlOpen('Gathering') then
            S.state.step = 0
        end
    end
end

function S.Cast()
    if IsControlOpen("GatheringMasterpiece") then
        d('Current Step '..S.state.step)
        if (S.rotations[S.state.step]) then
            S.rotations[S.state.step]()
        end
    end        
end

return S